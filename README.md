# blog

#### 介绍
{**lua-web博客介绍**
EventServer基于lua和libevent的完全异步、高性能的web框架。既可以作为游戏服务，也可以做web服务，支持websocket和https。

关键是可以ffi使用c/c++库，比如libcurl和iconv，也可以调用第三方lua库，比如smtp、redis、luasql等，采用委托线程执行的方式实现对io阻塞操作的异步调用，方便简单。
lua-web是基于EventServer的lua博客系统，代码就1000多行，有登录、注册、审核、发布、修改、删除、点赞、评论等功能。}

#### 软件架构
软件架构说明

依赖EventServer
依赖libevent
依赖LuaJit
依赖luasql、libcurl,iconv等


#### 安装教程

1. Windows直接下载运行即可 bin/gameserver.exe
2. Linux需要安装openssl、 libevent(修改）、luajit、mysql

#### 使用说明
1. 导入mysql数据库
Windows下，用workbench打开sql文件，执行
data/Dump20190828.sql

2. 修改bin/server.cfg
ip="127.0.0.1"
port=i0
lua="main.lua"
https=1
httpip="192.168.2.217"
httpport=i443
timer=5

####说明
https=1 
启用https，0-不启用

httpip="192.168.2.217"
web服务ip地址

httpport=i443
监听端口,https默认是443,http默认80.

lua="main.lua"
启动入口文件

默认接口
server_dispacher

3.执行
windows下
运行gameserver.exe

linux下，[需要自己下载](https://gitee.com/EventServer/EventServer.git)




#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


